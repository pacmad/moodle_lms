<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle_demo';
$CFG->dbuser    = 'postgres';
$CFG->dbpass    = '123456';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 5432,
  'dbsocket' => '',
);



$CFG->wwwroot   = 'http://localhost/moodle';
$CFG->dataroot  = 'C:\\xampp\\moodledata';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;
$CFG->behat_wwwroot = 'http://127.0.0.1/moodle';
$CFG->behat_dataroot = 'C:\\xampp\\htdocs\\behatdata';
$CFG->behat_prefix = 'behatda';
$CFG->behat_profiles = [
    'chrome' => [
        'browser' => 'chrome',
        'extensions' => [
            'Behat\MinkExtension' => [
                'selenium2' => [
                    'browser' => 'chrome',
                ]
            ]
        ]
    ]
];


$CFG->phpunit_prefix = 'phpu_';
$CFG->phpunit_dataroot = 'C:\\xampp\\htdocs\\phpu_moodledata';

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
