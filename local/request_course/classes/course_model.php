<?php

namespace local_request_course;

defined('MOODLE_INTERNAL') || die();

class course_model {

    public static $table = 'local_request_course';

    public static function create_request_course(\stdClass $data) {
        global $DB, $USER;

        return $DB->insert_record(self::$table, $data, true);
    }

    public static function get_courses(array $filter): array {
        $username = $filter['filterbycreator'] == 'all' ? '' : $filter['filterbycreator'];
        global $DB;

        $fields = 'a.id, a.title, a.content, a.userid, c.username as creator';

        $sql = "SELECT {$fields}
                FROM {local_request_course} a
                INNER JOIN {user} c ON a.userid = c.id";
        $param = [];
        if($filter['filterbycreator'] != 'all'){
            $sql .= ' WHERE username =  ?';
            array_push($param, $filter['filterbycreator']);
        }
        return $DB->get_records_sql($sql, $param);
    }

    public static function get_usernames(){
        global $DB;
        $fields = "a.username";
        $sql = "SELECT {$fields}
                FROM {user} a";
        return $DB->get_records_sql($sql);
    }
}