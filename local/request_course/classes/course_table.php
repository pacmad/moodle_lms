<?php


namespace local_request_course;

use local_request_course\output\manage_request_course;

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');

class course_table extends \table_sql {
    
    protected $sort;
  
    protected $id;

    public function __construct($records = null, array $option = []) {
        parent::__construct(constants::COURSE_UNIQUE_ID);
        $this->sort = array_key_exists('sort', $option) ? $option['sort'] : [];
        $this->id = array_key_exists('id', $option) ? $option['id'] : 0;
        $this->rawdata = $records;
    }


    public function out($pagesize = null, $useinitialsbar = false, $downloadhelpbutton = '') {
        $this->pagesize = is_null($pagesize) ? constants::DEFAULT_PAGE_SIZE : $pagesize;
        $this->init();
        $this->setup();
        $this->build_table();
        $this->finish_output();
    }

    
    public function col_checkbox($course) {
        return "<input type='checkbox' name='delete[]' value='$course->id' />";
    }

    public function col_title($course) {
        $out = \html_writer::span($course->title, 'course-title-text');
        return $out;
    }

    public function col_content($course) {
        $out = \html_writer::span($course->content, 'course-content');
        return $out;
    }

    public function col_creator($course) {
        $out = \html_writer::span($course->creator, 'course-creator');
        return $out;
    }


    protected function generate_url($sortfield, $sorttype = constants::SORT_ASC) {
        $sorttype = strtolower($sorttype);
        if ($this->sort['field'] == $sortfield && $this->sort['type'] == strtolower(constants::SORT_ASC)) {
            $sorttype = strtolower(constants::SORT_DESC);
        }
        return new \moodle_url('/local/request_course/local_manage_request_course_view.php', [
            'sortfield' => $sortfield,
            'sorttype' => $sorttype
        ]);
    }

    protected function init() {
        global $PAGE;
        $this->define_baseurl($PAGE->url);
        $this->is_downloadable(false);
        $this->collapsible(false);
        // $this->set_attribute('class', 'generaltable generalbox');
        $header = $this->generate_header();
        $this->define_headers($header);
        $this->define_columns([
                'checkbox',
                'title',
                'content',
                'creator'
        ]);
        $this->sortable(false);
        $this->pagesize($this->pagesize, count($this->rawdata));
    }

    protected function generate_header() {
        global $OUTPUT;

        $collapsedicon = \html_writer::img($OUTPUT->image_url("t/collapsed", 'moodle'), '', ['class' => "icon-collapsed"]);
        $expandedicon = \html_writer::img($OUTPUT->image_url("t/expanded", 'moodle'), '', ['class' => "icon-expanded"]);
        $type = \core_text::strtoupper($this->sort['type']);
        $field = \core_text::strtolower($this->sort['field']);

        if (empty($field)) {
            $field = 'timecreated';
        }

        $header = [
                \html_writer::img($OUTPUT->image_url('tick', 'local_request_course'), '', ['class' => 'icon-check']),
                \html_writer::link($this->generate_url('title'),
                        get_string('coursetitle', 'local_request_course')
                        . ' ' . (($field == 'title' && $type == constants::SORT_ASC) ? $expandedicon : $collapsedicon),
                        ['id' => 'sort-title', 'title' => get_string('sortbytitle', 'local_request_course'),
                            'class' => $field == 'title' ? 'actived' : '']),
                \html_writer::link($this->generate_url('content'),
                        get_string('content', 'local_request_course')
                        . ' ' . (($field == 'content' && $type == constants::SORT_ASC) ? $expandedicon : $collapsedicon),
                        ['id' => 'sort-content', 'content' => get_string('sortbycontent', 'local_request_course'),
                            'class' => $field == 'content' ? 'actived' : '']),
                \html_writer::link($this->generate_url('creator'),
                        get_string('creator', 'local_request_course')
                        . ' ' . (($field == 'creator' && $type == constants::SORT_ASC) ? $expandedicon : $collapsedicon),
                        ['id' => 'sort-creator', 'creator' => get_string('sortbycreator', 'local_request_course'),
                            'class' => $field == 'creator' ? 'actived' : '']),
        ];
        return $header;
    }
}