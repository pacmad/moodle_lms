<?php

namespace local_request_course;

use local_request_course\course_model;

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir.'/formslib.php');


class filter_form extends \moodleform {

    private function options_filter_init(){
        $options = course_model::get_usernames();
        $results = array();
        $results['all'] = 'All';
        foreach (array_keys($options) as $item){
            $results[$item] = $item;
        }
        return $results;
    }

    protected function definition() {
        $mform = $this->_form;

        $options = self::options_filter_init();

        $select = $mform->addElement('select', 'selected', get_string('filterbycreator', 'local_request_course'), $options);
        $this->add_action_buttons(true, get_string('filter', 'local_request_course'));
    }
}
