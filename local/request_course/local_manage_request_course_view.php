<?php

require_once('../../config.php');

$sortfield = optional_param('sortfield', null, PARAM_TEXT);
$sorttype = optional_param('sorttype', null, PARAM_TEXT);
$filterbycreator = optional_param('filterbycreator', 'all', PARAM_TEXT);

require_login();
$title = get_string('managerequestcourse', 'local_request_course');
$userid = $USER->id;
$context = context_user::instance($userid);
$PAGE->set_url('/local/request_course/local_manage_request_course_view.php');
$PAGE->set_context($context);
$PAGE->set_pagelayout('base');
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();

if (!has_capability('local/request_course:manage', $context, $userid)) {
    echo "You do not have permission to view this page";
    echo $OUTPUT->footer();
    die;
}

$mform = new local_request_course\filter_form(null, null, 'post', '', array('class' => 'myform'));

$mform->display();
if($fromform = $mform->get_data()){
    $filterbycreator = $fromform->selected;
}
$renderer = $PAGE->get_renderer('local_request_course');

echo $renderer->render_manage_course(['field' => $sortfield, 'type' => $sorttype], ['filterbycreator'=> $filterbycreator]);

echo $OUTPUT->footer();
