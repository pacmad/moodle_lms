@local_request_course
Feature: Best basic to view, filter, sort in manage course page.

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email              |
      | student1 | student   | 1        | student@gmail.com |
      | manager1 | manager   | jb        | abc@gmail.com     |
    And the following "system role assigns" exist:
      | user | role    | contextlevel |
      | student1 | student | System   |
      | manager1 | manager | System   |

  @javascript
  Scenario: Test student can not access to manage site
    Given I log in as "student1"
#    Prepare data
    When I click on "Request Course" "link"
    And I set the field "Title" to "1Test1"
    And I set the field "Content" to "Content"
    And I press "Add"
    And I should see "Add success"
    And I click on "Request Course" "link"
    And I set the field "Title" to "2Test2"
    And I set the field "Content" to "Content2"
    And I press "Add"
    And I should see "Add success"
    #    Test with student role
    When I click on "Manage Course" "link"
    Then I should see "You do not have permission to view this page"
    And I log out
    #   Test with manager role
    And I log in as "manager1"
    And I click on "Request Course" "link"
    And I set the field "Title" to "Test2"
    And I set the field "Content" to "Content2"
    And I press "Add"
    And I should see "Add success"

    #Test filter
    When I click on "Manage Course" "link"
    And I select "student1" from the "Filter by creator" singleselect
    And I press "Filter"
    And "1Test1" row "Creator" column of "table" table should contain "student1"
    And I select "manager1" from the "Filter by creator" singleselect
    And I press "Filter"
    And "Test2" row "Creator" column of "table" table should contain "manager1"

#    #Test sort
#    When I press "Link Title"
#    Then "1Test1" row "Creator" column of "table" table should contain "student1"
    And I pause