@local_request_course
Feature: Request a course pass re-captcha.

  Background:
    Given the following "users" exist:
      | username | firstname | lastname | email              |
      | student1 | student   | 1        | student@gmail.com |
      | manager1 | manager   | jb        | abc@gmail.com     |
    And the following "system role assigns" exist:
      | user | role    |
      | student1 | student |
      | manager1 | manager |

  @javascript
  Scenario: User submit valid request.
    Given I log in as "student1"
    When I click on "Request Course" "link"
    And I set the field "Title" to "Test1"
    And I set the field "Content" to "Content"
    And I press "Add"
    And I should see "Add success"
    And I log out
#    request course with role manager
    And I log in as "manager"
    When I click on "Request Course" "link"
    And I set the field "Title" to "Test2"
    And I set the field "Content" to "Content2"
    And I press "Add"
    And I should see "Add success"
    And I pause